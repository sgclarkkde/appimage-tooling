#!/usr/bin/env ruby
# frozen_string_literal: true
#
# Copyright (C) 2016 Scarlett Clark <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License fo-r more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.
require 'test/unit'
require 'yaml'
require 'fileutils'
require 'tty-command'
require_relative '../libs/sources'
require_relative '../libs/build'
require_relative '../libs/setup'
require_relative '../libs/builddocker.rb'

# Test yaml data
class TestSources < Test::Unit::TestCase
  def project_name
    Dir.chdir('/in')
    setup = Setup.new(
      File.join(File.dirname(__FILE__), 'data/metadata.yml')
    )
    setup.set_main_vars
    assert_equal('appimage-packaging', setup.metadata['name'])
  end

  def test_env
    Dir.chdir('/in')
    env = Setup.new(
      File.join(File.dirname(__FILE__), 'data/metadata.yml')
    )
    env.set_env
    assert_equal(
      '/opt/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      ENV.fetch('PATH')
    )
  end

  def test_data
    Dir.chdir('/in')
    setup = Setup.new(
      File.join(File.dirname(__FILE__), 'data/metadata.yml')
    )
    setup.set_main_vars
    assert_equal(setup.scm, 'git', setup.scm)
    setup.set_dep_vars(setup.dependencies[0])
    assert_equal(setup.depname, 'grantlee')
    setup.set_dep_vars(setup.dependencies[0])
    assert_equal(setup.scm, 'git')
    assert_equal(setup.build_kf5, true)
    assert_equal(setup.buildoptions, '-DCMAKE_INSTALL_PREFIX:PATH=/opt/usr CMAKE_BUILD_TYPE=Release')
  end

  def test_apt
    Dir.chdir('/in')
    setup = Setup.new(
      File.join(File.dirname(__FILE__), 'data/metadata.yml')
    )
    setup.set_main_vars
    setup.install_packages(setup.packages)
    assert(setup.status.to_s.include?('exit 0'))
    assert(system('dpkg -l qt5-qmake'))
  end

  def test_fetch_tarball
    fetch = Sources.new('/app/src')
    fetch.fetch_tarball('https://download.kde.org/stable/applications/18.08.2/src/akonadi-notes-18.08.2.tar.xz')
    assert(File.file?('akonadi-notes-18.08.2.tar.xz'), 'Nothing was downloaded')
    assert(fetch.check_md5sum('akonadi-notes-18.08.2.tar.xz', '485fa04d3d46be2188fa262e5ee53654'), 'md5sum failed')
    FileUtils.rm('akonadi-notes-18.08.2.tar.xz')
  end

  def test_git
    clone = Sources.new('/app/src')
    clone.clone_git('https://anongit.kde.org/akonadi-notes', 'Applications/18.08')
    assert(File.exist?('/app/src/akonadi-notes'), 'Git clone failed')
    FileUtils.rm_rf('/app/src/akonadi-notes')
  end

  def test_unpack_tarball
    unpack = Sources.new('/app/src')
    unpack.fetch_tarball('https://download.kde.org/stable/applications/18.08.2/src/akonadi-notes-18.08.2.tar.xz')
    assert(File.file?('akonadi-notes-18.08.2.tar.xz'), 'Nothing was downloaded')
    system('file akonadi-notes-18.08.2.tar.xz')
    assert_equal(File.extname('akonadi-notes-18.08.2.tar.xz').delete('.'), 'xz')
    unpack.unpack_tarball('akonadi-notes-18.08.2.tar.xz', 'akonadi-notes-18.08.2')
    assert(File.exist?('/app/src/akonadi-notes-18.08.2'), 'Unpack failed')
    Build.new('/app/src/akonadi-notes-18.08.2', false, true)
    cmd = TTY::Command.new
    working_dir = cmd.run('pwd')
    assert(working_dir, '/app/src/akonadi-notes-18.08.2')
    assert(File.exist?('/app/src/akonadi-notes-18.08.2/CMakeLists.txt'))
    FileUtils.rm('/app/src/akonadi-notes-18.08.2.tar.xz')
    FileUtils.rm_rf('akonadi-notes-18.08.2')
  end

  def test_in_source
    clone = Sources.new('/app/src')
    insource = true
    clone.clone_git('https://anongit.kde.org/akonadi-notes')
    Build.new('/app/src/akonadi-notes', false, insource)
    assert(File.dirname(__FILE__), 'akonadi-notes')
    insource = false
    Build.new('/app/src/akonadi-notes', false, insource)
    assert(File.dirname(__FILE__), 'build')
    FileUtils.rm_rf('/app/src/akonadi-notes')
  end

  def test_cmake_build
    Dir.chdir('/in')
    data = Setup.new(
      File.join(__dir__, 'data/metadata.yml')
    )
    data.set_main_vars
    insource = true
    options = data.buildoptions
    buildsystem = data.buildsystem
    Sources.new('/app/src')
    FileUtils.cp_r('/in/tests/data/cmake/.', '/app/src/cmake')
    build = Build.new('/app/src/cmake', false, insource)
    build.run_build(buildsystem, options)
    assert(build.status.to_s.include?('exit 0'))
    FileUtils.rm_rf('/app/src/cmake')
  end

    def test_make_build
      Dir.chdir('/in')
      insource = true
      options = '--prefix=/opt/usr'
      Sources.new('/app/src')
      FileUtils.cp_r('/in/tests/data/make/.', '/app/src/make')
      FileUtils.chmod 0755, '/app/src/make/configure'
      build = Build.new('/app/src/make', false, insource)
      build.run_build('make', options)
      assert(build.status.to_s.include?('exit 0'))
      FileUtils.rm_rf('/app/src/make')
    end

    def test_qmake_build
      Dir.chdir('/in')
      insource = true
      options = 'PREFIX += /opt/usr'
      Sources.new('/app/src')
      FileUtils.cp_r('/in/tests/data/qmake/.', '/app/src/qmake')
      build = Build.new('/app/src/qmake', false, insource)
      build.build_qmake(options, 'hello.pro')
      build.build_make
      assert(build.status.to_s.include?('exit 0'))
      FileUtils.rm_rf('/app/src/make')
    end

    def test_autoconf_build
      Dir.chdir('/in')
      insource = false
      options = '--prefix=/opt/usr'
      Sources.new('/app/src')
      FileUtils.cp_r('/in/tests/data/autoconf/.', '/app/src/autoconf')
      build = Build.new('/app/src/autoconf', false, insource)
      build.build_autoconf
      assert(File.exist?('/app/src/autoconf/configure'), 'Autoconf failed')
      build.build_configure(options)
      build.build_make
      assert(build.status.to_s.include?('exit 0'))
      FileUtils.rm_rf('/app/src/autoconf')
    end

    def test_custom_build
      Dir.chdir('/in')
      insource = true
      options = 'sh hello.sh'
      Sources.new('/app/src')
      FileUtils.cp_r('/in/tests/data/custom/.', '/app/src/custom')
      build = Build.new('/app/src/custom', false, insource)
      build.build_custom(options)
      assert(build.status.to_s.include?('exit 0'))
      FileUtils.rm_rf('/app/src/custom')
    end

    def test_apply_patch
      Dir.chdir('/in')
      insource = true
      options = '-DCMAKE_INSTALL_PREFIX=/opt/usr'
      Sources.new('/app/src')
      FileUtils.cp_r('/in/tests/data/cmake/.', '/app/src/cmake')
      FileUtils.cp_r('/in/tests/patches/.', '/in/patches')
      build = Build.new('/app/src/cmake', true, insource)
      build.apply_patch('patches/test.patch')
      build.run_build('cmake', options)
      assert(build.status.to_s.include?('exit 0'))
      FileUtils.rm_rf('/app/src/cmake')
    end
end
