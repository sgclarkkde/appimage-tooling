#!/usr/bin/env ruby
# frozen_string_literal: true

# Copyright (C) 2016 Scarlett Moore <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

# Set environment and project variables
require 'English'
require 'tty-command'

# Set project variables from metadata yaml file.
class Setup
  attr_accessor :metadata
  attr_accessor :name
  attr_accessor :binary
  attr_accessor :scm
  attr_accessor :url
  attr_accessor :branch
  attr_accessor :packages
  attr_accessor :dependencies
  attr_accessor :build_kf5
  attr_accessor :build_kf5_deps
  attr_accessor :frameworks
  attr_accessor :kf5_deps
  attr_accessor :buildsystem
  attr_accessor :buildoptions
  attr_accessor :insource
  attr_accessor :depname
  attr_accessor :profile
  attr_accessor :status
  attr_accessor :md5sum
  attr_accessor :need_patches
  attr_accessor :patches

  def initialize(file)
    @metadata = YAML.load_file(file)
  end

  # Set variables main project variables.
  def set_main_vars
    @name = @metadata['name']
    @binary = @metadata['binary']
    @scm = @metadata['scm']
    @url = @metadata['url']
    @packages = @metadata['packages']
    @dependencies = @metadata['dependencies']
    @build_kf5 = @metadata['build_kf5']
    @build_kf5_deps = @metadata['build_kf5_deps']
    @frameworks = @metadata['frameworks']
    @kf5_deps = @metadata['kf5_deps']
    @buildsystem = @metadata['buildsystem']
    @buildoptions = @metadata['buildoptions']
    @insource = @metadata['insource']
    @profile = @metadata['profile']
    @md5sum = @metadata['md5sum']
    @need_patches = @metadata['need_patches']
    @patches = @metadata['patches']
  end

  def set_dep_vars(dep)
    @depname = dep['depname']
    @scm = dep['scm']
    @url = dep['url']
    @branch = dep['branch']
    @buildsystem = dep['buildsystem']
    @buildoptions = dep['buildoptions']
    @insource = dep['insource']
    @profile = dep['profile']
    @md5sum = dep['md5sum']
    @need_patches = dep['need_patches']
    @patches = dep['patches']
  end

  # Set the environment
  def set_env
    ENV['PATH'] = '/opt/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    ENV['LD_LIBRARY_PATH'] = '/opt/usr/lib:/opt/usr/lib/x86_64-linux-gnu:/usr/lib:/usr/lib/x86_64-linux-gnu:/usr/lib64:/usr/lib:/lib:/lib64'
    ENV['CPATH'] = '/opt/usr:/opt/usr/include:/usr/include'
    ENV['CFLAGS'] = '-g -O2 -fPIC'
    ENV['CXXFLAGS'] = '-std=c++11'
    ENV['PKG_CONFIG_PATH'] = '/opt/usr/lib/pkgconfig:/opt/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig'
    ENV['ACLOCAL_PATH'] = '/opt/usr/share/aclocal:/usr/share/aclocal'
    ENV['XDG_DATA_DIRS'] = '/opt/usr/share:/opt/share:/usr/local/share/:/usr/share:/share'
    ENV['DEBIAN_FRONTEND'] = 'noninteractive'
    ENV['QT_SELECT'] = 'qt5'
    puts ENV['PATH']
    puts ENV['LD_LIBRARY_PATH']
    puts ENV['CPATH']
    puts ENV['CFLAGS']
    puts ENV['CXXFLAGS']
    puts ENV['PKG_CONFIG_PATH']
    puts ENV['ACLOCAL_PATH']
    puts ENV['XDG_DATA_DIRS']
  end

  def install_packages(packages)
    cmd = TTY::Command.new
    cmd.run('sudo apt-get update')
    @status = $CHILD_STATUS
    cmd.run('sudo apt-get -y upgrade')
    @status = $CHILD_STATUS
    if packages
      packages = packages.join(' ')
      puts "Running sudo apt-get -y install #{packages}"
      cmd.run('sudo apt-get -y install ' + packages)
      @status = $CHILD_STATUS
    else
      puts 'No packages'
      @status = '0'
    end
  end
end
