#!/usr/bin/env ruby
# frozen_string_literal: true
#
# Copyright (C) 2016 Scarlett Moore <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.
require 'yaml'
require 'fileutils'
require_relative 'urlresolver'
require 'open3'
require 'tty-command'
require 'digest/md5'
require_relative 'retry'

# Retrieve sources
class Sources
  attr_accessor :branch

  def initialize(working_dir)
    unless File.exist?(working_dir)
      FileUtils.mkdir_p(working_dir)
    end
    Dir.chdir(working_dir)
  end

  def check_md5sum(tarball, md5sum)
    downloaded = Digest::MD5.file(tarball).to_s
    site = md5sum
    puts 'Downloaded: ' + downloaded
    puts 'Site: ' + site
    downloaded == site
  end

  def fetch_tarball(url)
    name = File.basename(url)
    Retry.retry_it(times: 5, sleep: 4) do
      get_file = UrlResolver.new
      response = get_file.resolve(url)
      File.open(name, 'w+') do |file|
        file.write(response) # write the contents
      end
    end
  end

  def unpack_tarball(file, name)
    type = File.extname(file).delete('.')
    FileUtils.mkdir(name)
    case type
    when 'gz'
      Open3.popen2('tar', '-zxvf', file, '-C', name, '--strip-components=1') do |stdin, stdout, status_thread|
        stdout.each_line do |line|
          puts "LINE: #{line}"
        end
        raise 'Untar failed'  unless status_thread.value.success?
      end
    when 'xz'
      Open3.popen2('tar', '-xvf', file, '-C', name, '--strip-components=1') do |stdin, stdout, status_thread|
        stdout.each_line do |line|
          puts "LINE: #{line}"
        end
        raise 'Untar failed'  unless status_thread.value.success?
      end
    when 'bz2'
      Open3.popen2('tar', '-jxvf', file, '-C', name, '--strip-components=1') do |stdin, stdout, status_thread|
        stdout.each_line do |line|
          puts "LINE: #{line}"
        end
        raise 'Untar failed'  unless status_thread.value.success?
      end
    end
  end

  def clone_git(url, branch = 'master')
    name = File.basename(url)
    cmd = TTY::Command.new
    Retry.retry_it(times: 5, sleep: 4) do
      cmd.run("git clone #{url} /app/src/#{name}")
    end
    Dir.chdir("/app/src/#{name}")
    cmd.run("git checkout #{branch}")
    puts branch
  end
end


#     when 'xz'
#       Dir.chdir('/app/src/')
#       unless Dir.exist?("/app/src/#{name}")
#         system("wget #{url}")
#         system("tar -xvf #{name}*.tar.xz")
#       end
#     when 'gz'
#       Dir.chdir('/app/src/')
#       unless Dir.exist?("/app/src/#{name}")
#         system("wget #{url}")
#         system("tar -zxvf #{name}*.tar.gz")
#       end
#     when 'bz2'
#       Dir.chdir('/app/src/')
#       unless Dir.exist?("/app/src/#{name}")
#         system("wget #{url}")
#         system("tar -jxvf #{name}.tar.bz2")
#       end
#     when 'mercurial'
#       Dir.chdir('/app/src')
#       unless Dir.exist?("/app/src/#{name}")
#         system("hg clone #{url}")
#       end
#     when 'bzr'
#       Dir.chdir('/app/src')
#       unless Dir.exist?("/app/src/#{name}")
#         system("bzr branch #{url}")
#       end
#     when 'zip'
#       Dir.chdir('/app/src')
#       unless Dir.exist?("/app/src/#{name}")
#         system("wget #{url}")
#         system("unzip #{name}.zip")
#       end
#     when 'svn'
#       Dir.chdir('/app/src')
#       unless Dir.exist?("/app/src/#{name}")
#         system("svn export #{url}")
#       end
#     when 'none'
#       Dir.chdir('/app/src')
#       unless Dir.exist?("/app/src/#{name}")
#         Dir.mkdir "#{name}"
#         p "No sources configured"
#       end
#     else
#       "You gave me #{type} -- I have no idea what to do with that."
#     end
#     $?.exitstatus
#   end
# end
