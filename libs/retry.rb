# frozen_string_literal: true

# Copyright (C) 2015-2016 Harald Sitter <sitter@kde.org>
#                         2019 Scarlett Moore <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.
# Helps with retrying an exception throwing code.
module Retry
  module_function

  def disable_sleeping
    @sleep_disabled = true
  end

  def enable_sleeping
    @sleep_disabled = false
  end

  # Retry given block.
  # @param tries [Integer] amount of tries
  # @param errors [Array<Object>] errors to rescue
  # @param sleep [Integer, nil] seconds to sleep between tries
  # @param name [String, 'unknown'] name of the action (debug when not silent)
  # @yield yields to block which needs retrying
  def retry_it(times: 1, errors: [StandardError], sleep: nil, silent: false,
               name: 'unknown')
    yield
  rescue *errors => e
    raise e if (times -= 1) <= 0

    print "Error on retry_it(#{name}) :: #{e}\n" unless silent
    Kernel.sleep(sleep) if sleep && !@sleep_disabled
    retry
  end
end
